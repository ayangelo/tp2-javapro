import objects.BankUser;
import objects.CreditAgricool;
import objects.Fortunement;
import objects.IBank;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InjectionTest {

    @Test
    @DisplayName("Injection par constructeur:CreditAgricool")
    public void Test1() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, CreditAgricool.class);

        BankUser user = container.newInstance(BankUser.class);

        assertNotNull(user.getBank());

        assertTrue(user.getBank() instanceof CreditAgricool);
        assertEquals(500, user.getAccount().getBalance());
    }

    @Test
    @DisplayName("Injection par attribut:CreditAgricool")
    public void Test2() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, CreditAgricool.class);

        BankUser user = container.newInstance(BankUser.class);

        assertNotNull(user.getBank2());

        assertTrue(user.getBank2() instanceof CreditAgricool);
        assertEquals(500, user.getAccount().getBalance());
    }

    @Test
    @DisplayName("Injection par constructeur:Fortunement")
    public void Test3() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, Fortunement.class);

        BankUser user = container.newInstance(BankUser.class);

        assertNotNull(user.getBank());

        assertTrue(user.getBank() instanceof Fortunement);
        assertEquals(550, user.getAccount().getBalance()); //Vérification du bonus de 50 de la banque Fortunement
    }

    @Test
    @DisplayName("Injection par attribut:Fortunement")
    public void Test4() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, Fortunement.class);

        BankUser user = container.newInstance(BankUser.class);

        assertNotNull(user.getBank2());

        assertTrue(user.getBank2() instanceof Fortunement);
        assertEquals(550, user.getAccount().getBalance());
    }

    @Test
    @DisplayName("Même instance pour les deux injections:CreditAgricool")
    public void Test5() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, CreditAgricool.class);

        BankUser user = container.newInstance(BankUser.class);

        assertEquals(user.getBank(), user.getBank2());
    }

    @Test
    @DisplayName("Même instance pour les deux injections:Fortunement")
    public void Test6() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, Fortunement.class);

        BankUser user = container.newInstance(BankUser.class);

        assertEquals(user.getBank(), user.getBank2());
    }

    @Test
    @DisplayName("Multiple injections par attribut:CreditAgricool")
    public void Test7() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, CreditAgricool.class);

        BankUser user = container.newInstance(BankUser.class);

        assertNotNull(user.getBank3());
        assertTrue(user.getBank3() instanceof CreditAgricool);
        assertEquals(user.getBank2(), user.getBank3());
    }

    @Test
    @DisplayName("Vérification qu'un attribut non annoté n'est pas injecté")
    public void Test8() throws InstantiationException, IllegalAccessException {
        Container container = new Container();
        container.register(IBank.class, CreditAgricool.class);

        BankUser user = container.newInstance(BankUser.class);

        assertNotNull(user.getBank());
        assertNull(user.getBank4());
    }
}
