package objects;

import java.util.ArrayList;
import java.util.List;

public class CreditAgricool implements IBank {
    private final List<IAccount> accounts = new ArrayList<>();

    public List<? extends IAccount> getAccounts() {
        return accounts;
    }

    public IAccount newAccount(double initialBalance) {
        IAccount account = new Account(initialBalance);
        accounts.add(account);
        return account;
    }

    public List<String> getOperations(IAccount account) {
        return account.getOperations();
    }

    public void transfer(IAccount from, IAccount to, double amount) {
        if (accounts.contains(from)) {
            from.setBalance(from.getBalance() - amount);
            to.setBalance(to.getBalance() + amount);
        }
    }
}
