package objects;

import java.util.ArrayList;
import java.util.List;

public class Account implements IAccount {
    private double balance;
    private final List<String> operations = new ArrayList<String>();

    public Account(double initialBalance) {
        balance = 0;
        setBalance(initialBalance);
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double newBalance) {
        double difference = newBalance - balance;
        String sign = (difference >= 0) ? "+" : "-";
        operations.add(sign + String.valueOf(difference));
        balance = newBalance;
    }

    public List<String> getOperations() {
        return operations;
    }

}
