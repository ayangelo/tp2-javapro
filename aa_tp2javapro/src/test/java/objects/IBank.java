package objects;

import java.util.List;

public interface IBank {
    List<? extends IAccount> getAccounts();
    IAccount newAccount(double initialBalance);
    List<String> getOperations(IAccount account);
    void transfer(IAccount from, IAccount to, double amount);
}
