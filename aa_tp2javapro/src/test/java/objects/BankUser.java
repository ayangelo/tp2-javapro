package objects;

import annotations.InjectByConstructor;
import annotations.InjectByField;

public class BankUser {

    private String name;
    private final IAccount account;
    private final IBank bank;

    @InjectByField
    private IBank bank2;

    @InjectByField
    private IBank bank3;
    private IBank bank4;

    @InjectByConstructor
    public BankUser(IBank bank) {
        this.bank = bank;
        this.account = bank.newAccount(500);
        setName("ayangelo");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public IAccount getAccount() {
        return account;
    }

    public IBank getBank() {
        return bank;
    }

    public IBank getBank2() {
        return bank2;
    }

    public IBank getBank3() {
        return bank3;
    }

    public IBank getBank4() {
        return bank4;
    }

    public void pay(IAccount account, double amount) {
        bank.transfer(this.account, account, amount);
    }

    public void payWithBank2(IAccount account, double amount) {
        bank2.transfer(this.account, account, amount);
    }
}
