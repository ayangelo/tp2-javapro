package objects;

import java.util.List;

public interface IAccount {
    public double getBalance();
    public void setBalance(double newBalance);
    public List<String> getOperations();
}
