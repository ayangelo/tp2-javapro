import java.lang.reflect.InvocationTargetException;

public interface IContainer {
    void register(Class<?> classInterface, Class<?> dependency);
    <T> T newInstance(Class<T> dependency) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;
}
