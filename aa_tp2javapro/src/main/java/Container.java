import annotations.InjectByConstructor;
import annotations.InjectByField;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Container implements IContainer {
    private final Map<Class<?>, Class<?>> dependencies = new HashMap<>();
    private final Map<Class<?>, Object> instances = new HashMap<>();

    public void register(Class<?> abstraction, Class<?> implementation) {
        dependencies.put(abstraction, implementation);
    }

    @SuppressWarnings({"unchecked"})
    public <T> T newInstance(Class<T> abstraction) throws InstantiationException, IllegalAccessException {

        if (instances.containsKey(abstraction)) {
            return (T) instances.get(abstraction);
        }

        Class<T> implementation = (Class<T>) dependencies.getOrDefault(abstraction, abstraction);

        T finalInstance = null;

        // Injection by constructor

        List<Constructor<?>> injectableConstructors = Arrays.stream(implementation.getConstructors())
                .filter(constructor -> constructor.getAnnotation(InjectByConstructor.class) != null)
                .collect(Collectors.toList());

        if (!injectableConstructors.isEmpty()) {
            Constructor<?> constructor = injectableConstructors.get(0);
            List<Object> args = Arrays.stream(constructor.getParameterTypes())
                    .collect(Collectors.toList());

            if (!args.isEmpty()) {
                args = args.stream()
                        .map(arg -> {
                            Class<?> argImplementation = dependencies.get((Class<?>)arg);
                            try {
                                if (!instances.containsKey(arg.getClass())) {
                                    Object instance = argImplementation.newInstance();
                                    instances.put((Class<?>)arg, instance);
                                    return instance;
                                }
                                else {
                                    return instances.get(arg.getClass());
                                }

                            }
                            catch (InstantiationException | IllegalAccessException | NullPointerException e) {
                                e.printStackTrace();
                                return null;
                            }
                        })
                        .collect(Collectors.toList());
            }

            try {
                finalInstance = (T) constructor.newInstance(args.toArray());
            }
            catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if (finalInstance == null) {
            finalInstance = implementation.newInstance();
        }

        // Injection by field

        List<Field> injectableFields = Arrays.stream(implementation.getDeclaredFields())
                .filter(field -> field.getAnnotation(InjectByField.class) != null)
                .collect(Collectors.toList());

        if (!injectableFields.isEmpty()) {
            for (Field field : injectableFields) {
                Class<?> fieldImplementation = dependencies.get(field.getType());
                field.setAccessible(true);
                try {

                    if (!instances.containsKey(field.getType())) {
                        Object instance = fieldImplementation.newInstance();
                        field.set(finalInstance, instance);
                        instances.put(field.getType(), instance);
                    }
                    else {
                        field.set(finalInstance, instances.get(field.getType()));
                    }
                } catch (IllegalAccessException |
                        InstantiationException |
                        NullPointerException |
                        IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }

        return finalInstance;
    }
}


