# TP DI-Container

Aymeric ANGELO F2

## Fonctionnement :

```java
@InjectByConstructor // Les constructeurs annotés recourent à l'injection pour instancier les attributs en paramètre du constructeur
```

```java
@InjectByField // Les attributs annotés recourent à l'injection pour s'instancier
```

Le Container est testé sur un système bancaire de plusieurs banques : CreditAgricool et Fortunement
